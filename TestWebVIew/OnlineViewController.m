//
//  OnlineViewController.m
//  TestWebVIew
//
//  Created by shaowei on 13-10-23.
//  Copyright (c) 2013年 AISpeech-SW. All rights reserved.
//

#import "OnlineViewController.h"
#import "VideoViewController.h"

@interface OnlineViewController ()

@property (nonatomic ,copy) NSString *m3u8String;

@end

@implementation OnlineViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self loadURL:@"http://www.youku.com"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_m3u8String release];
    [_webView release];
    [_textField release];
    [_labelOfM3u8 release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setWebView:nil];
    [self setTextField:nil];
    [self setLabelOfM3u8:nil];
    [super viewDidUnload];
}

- (void)setM3u8String:(NSString *)m3u8String{
    if (_m3u8String != m3u8String) {
        [_m3u8String release];
        _m3u8String = [m3u8String retain];
    }
    
    self.labelOfM3u8.text = _m3u8String;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.labelOfM3u8.text = nil;
}


#pragma mark - action

- (IBAction)actionGo:(id)sender {
    [self actionCancel:nil];
    NSString *urlString = self.textField.text;
    if (![urlString hasPrefix:@"http"]) {
        urlString = [@"http://" stringByAppendingString:urlString];
        self.textField.text = urlString;
    }
    [self loadURL:urlString];
    
    
}
- (IBAction)actionCancel:(id)sender {
    [self.textField resignFirstResponder];
}

- (IBAction)actionPlay:(id)sender {
    
    if (self.m3u8String.length) {
        [self playVideoWithURL:self.m3u8String];
    }else{
        NSLog(@"SW - m3u8为空，不能播放");
    }

}

- (IBAction)actionParseM3u8:(id)sender {
    [self parseM3u8From:self.webView];
}



#pragma mark Function - Private

- (void)loadURL:(NSString *)urlString{
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
}

- (NSString *)parseM3u8From:(UIWebView *)webView{
    NSString *lJs2 = @"(document.getElementsByTagName(\"video\")[0]).src"; // youku,letv,souhu pptv
    NSString *lm3u8 = [webView stringByEvaluatingJavaScriptFromString:lJs2];
    if (lm3u8) {
        self.m3u8String = lm3u8;
    }
    
    return lm3u8;
    
    
}

- (void)playVideoWithURL:(NSString *)m3u8String{
    
    if (self.navigationController.visibleViewController == self) {
        VideoViewController *videoVC = [[VideoViewController alloc] initWithNibName:nil
                                                                             bundle:nil];
        [self.navigationController pushViewController:videoVC animated:NO];
        [videoVC playVideo:m3u8String];
        [videoVC release];
        NSLog(@"开始播放video：%@",m3u8String);
    }else{
        NSLog(@"阻隔---已经有video正在播放");
    }
    
}

#pragma mark - delegate

- (void)webViewDidStartLoad:(UIWebView *)webView{
    NSString *m3u8 = [self parseM3u8From:webView];
    NSLog(@"start-m3u8:%@request:%@",m3u8,webView.request.URL.absoluteString);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    NSString *m3u8 = [self parseM3u8From:webView];
    if (m3u8.length) {
        NSLog(@"finish-\n   m3u8:%@,\nrequest:%@",m3u8,webView.request.URL.absoluteString);
    }else{
        NSLog(@"finish-m3u8:%@",m3u8);
    }
    
    
    
//    if (m3u8.length > 0) {
//        [self playVideoWithURL:m3u8];
//    }
    
}







@end
