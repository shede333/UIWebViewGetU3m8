//
//  ViewController.h
//  TestWebVIew
//
//  Created by shaowei on 13-10-18.
//  Copyright (c) 2013年 AISpeech-SW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (retain, nonatomic) IBOutlet UIWebView *webView;


@end
