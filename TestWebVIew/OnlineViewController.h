//
//  OnlineViewController.h
//  TestWebVIew
//
//  Created by shaowei on 13-10-23.
//  Copyright (c) 2013年 AISpeech-SW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineViewController : UIViewController<UIWebViewDelegate>

@property (retain, nonatomic) IBOutlet UIWebView *webView;

@property (retain, nonatomic) IBOutlet UITextField *textField;
@property (retain, nonatomic) IBOutlet UILabel *labelOfM3u8;

- (IBAction)actionGo:(id)sender;


@end
